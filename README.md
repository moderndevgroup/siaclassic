# [![SiaClassic Logo](http://siaclassic.tech/img/svg/siaclassic-green-logo.svg)](http://siaclassic.tech) v1.3.4.1 (Capricorn)

[![Build Status](https://travis-ci.org/moderndevgroup/SiaClassic.svg?branch=master)](https://travis-ci.org/moderndevgroup/SiaClassic)
[![GoDoc](https://godoc.org/gitlab.com/moderndevgroup/SiaClassic?status.svg)](https://godoc.org/gitlab.com/moderndevgroup/SiaClassic)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/moderndevgroup/SiaClassic)](https://goreportcard.com/report/gitlab.com/moderndevgroup/SiaClassic)

SiaClassic is a new decentralized cloud storage platform that radically alters the
landscape of cloud storage. By leveraging smart contracts, client-side
encryption, and sophisticated redundancy (via Reed-Solomon codes), SiaClassic allows
users to safely store their data with hosts that they do not know or trust.
The result is a cloud storage marketplace where hosts compete to offer the
best service at the lowest price. And since there is no barrier to entry for
hosts, anyone with spare storage capacity can join the network and start
making money.

![UI](http://i.imgur.com/iHoGqoL.png)

Traditional cloud storage has a number of shortcomings. Users are limited to a
few big-name offerings: Google, Microsoft, Amazon. These companies have little
incentive to encrypt your data or make it easy to switch services later. Their
code is closed-source, and they can lock you out of your account at any time.

We believe that users should own their data. SiaClassic achieves this by replacing
the traditional monolithic cloud storage provider with a blockchain and a
swarm of hosts, each of which stores an encrypted fragment of your data. Since
the fragments are redundant, no single host can hold your data hostage: if
they jack up their price or go offline, you can simply download from a
different host. In other words, trust is removed from the equation, and
switching to a different host is painless. Stripped of these unfair
advantages, hosts must compete solely on the quality and price of the storage
they provide.

SiaClassic can serve as a replacement for personal backups, bulk archiving, content
distribution, and more. For developers, SiaClassic is a low-cost alternative to
Amazon S3. Storage on SiaClassic is a full order of magnitude cheaper than on S3,
with comparable bandwidth, latency, and durability. SiaClassic works best for static
content, especially media like videos, music, and photos.

Distributing data across many hosts automatically confers several advantages.
The most obvious is that, just like BitTorrent, uploads and downloads are
highly parallel. Given enough hosts, SiaClassic can saturate your bandwidth. Another
advantage is that your data is spread across a wide geographic area, reducing
latency and safeguarding your data against a range of attacks.

It is important to note that users have full control over which hosts they
use. You can tailor your host set for minimum latency, lowest price, widest
geographic coverage, or even a strict whitelist of IP addresses or public
keys.

At the core of SiaClassic is a blockchain that closely resembles Bitcoin.
Transactions are conducted in SiaClassiccoin, a cryptocurrency. The blockchain is
what allows SiaClassic to enforce its smart contracts without relying on centralized
authority. To acquire siaclassiccoins, use an exchange such as [Bittrex](https://bittrex.com), [Yunbi](https://yunbi.com), or
[Bisq](https://www.bisq.io).

To get started with SiaClassic, check out the guides below:

- [How to Store Data on SiaClassic](https://blog.siaclassic.tech/getting-started-with-private-decentralized-cloud-storage-c9565dc8c854)
- [How to Become a SiaClassic Host](https://blog.siaclassic.tech/how-to-run-a-host-on-siaclassic-2159ebc4725)
- [Using the SiaClassic API](https://blog.siaclassic.tech/api-quickstart-guide-f1d160c05235)


Usage
-----

SiaClassic is ready for use with small sums of money and non-critical files, but
until the network has a more proven track record, we advise against using it
as a sole means of storing important data.

This release comes with 2 binaries, siaclassicd and siaclassicc. siaclassicd is a background
service, or "daemon," that runs the SiaClassic protocol and exposes an HTTP API on
port 7780. siaclassicc is a command-line client that can be used to interact with
siaclassicd in a user-friendly way. There is also a graphical client, [SiaClassic-UI](https://gitlab.com/moderndevgroup/SiaClassic-UI), which
is the preferred way of using SiaClassic for most users. For interested developers,
the siaclassicd API is documented [here](doc/API.md).

siaclassicd and siaclassicc are run via command prompt. On Windows, you can just double-
click siaclassicd.exe if you don't need to specify any command-line arguments.
Otherwise, navigate to its containing folder and click File->Open command
prompt. Then, start the siaclassicd service by entering `siaclassicd` and pressing Enter.
The command prompt may appear to freeze; this means siaclassicd is waiting for
requests. Windows users may see a warning from the Windows Firewall; be sure
to check both boxes ("Private networks" and "Public networks") and click
"Allow access." You can now run `siaclassicc` (in a separate command prompt) or SiaClassic-
UI to interact with siaclassicd. From here, you can send money, upload and download
files, and advertise yourself as a host.

Building From Source
--------------------

To build from source, [Go 1.10 must be installed](https://golang.org/doc/install)
on the system. Make sure your `$GOPATH` is set, and then simply use `go get`:

```
go get -u gitlab.com/moderndevgroup/SiaClassic/...
```

This will download the SiaClassic repo to your `$GOPATH/src` folder and install the
`siaclassicd` and `siaclassicc` binaries in your `$GOPATH/bin` folder.

To stay up-to-date, run the previous `go get` command again. Alternatively, you
can use the Makefile provided in this repo. Run `git pull origin master` to
pull the latest changes, and `make release` to build the new binaries. You
can also run `make test` and `make test-long` to run the short and full test
suites, respectively. Finally, `make cover` will generate code coverage reports
for each package; they are stored in the `cover` folder and can be viewed in
your browser.

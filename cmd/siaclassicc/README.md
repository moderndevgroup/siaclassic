SiaClassicc Usage
==========

`siaclassicc` is the command line interface to SiaClassic, for use by power users and
those on headless servers. It comes as a part of the command line
package, and can be run as `./siaclassicc` from the same folder, or just by
calling `siaclassicc` if you move the binary into your path.

Most of the following commands have online help. For example, executing
`siaclassicc wallet send help` will list the arguments for that command,
while `siaclassicc host help` will list the commands that can be called
pertaining to hosting. `siaclassicc help` will list all of the top level
command groups that can be used.

You can change the address of where siaclassicd is pointing using the `-a`
flag. For example, `siaclassicc -a :9000 status` will display the status of
the siaclassicd instance launched on the local machine with `siaclassicd -a :9000`.

Common tasks
------------
* `siaclassicc consensus` view block height

Wallet:
* `siaclassicc wallet init [-p]` initilize a wallet
* `siaclassicc wallet unlock` unlock a wallet
* `siaclassicc wallet balance` retrieve wallet balance
* `siaclassicc wallet address` get a wallet address
* `siaclassicc wallet send [amount] [dest]` sends siaclassiccoin to an address

Renter:
* `siaclassicc renter list` list all renter files
* `siaclassicc renter upload [filepath] [nickname]` upload a file
* `siaclassicc renter download [nickname] [filepath]` download a file


Full Descriptions
-----------------

#### Wallet tasks

* `siaclassicc wallet init [-p]` encrypts and initializes the wallet. If the
`-p` flag is provided, an encryption password is requested from the
user. Otherwise the initial seed is used as the encryption
password. The wallet must be initialized and unlocked before any
actions can be performed on the wallet.

Examples:
```bash
user@hostname:~$ siaclassicc -a :9920 wallet init
Seed is:
 cider sailor incur sober feast unhappy mundane sadness hinder aglow imitate amaze duties arrow gigantic uttered inflamed girth myriad jittery hexagon nail lush reef sushi pastry southern inkling acquire

Wallet encrypted with password: cider sailor incur sober feast unhappy mundane sadness hinder aglow imitate amaze duties arrow gigantic uttered inflamed girth myriad jittery hexagon nail lush reef sushi pastry southern inkling acquire
```

```bash
user@hostname:~$ siaclassicc -a :9920 wallet init -p
Wallet password:
Seed is:
 potato haunted fuming lordship library vane fever powder zippers fabrics dexterity hoisting emails pebbles each vampire rockets irony summon sailor lemon vipers foxes oneself glide cylinder vehicle mews acoustic

Wallet encrypted with given password
```

* `siaclassicc wallet unlock` prompts the user for the encryption password
to the wallet, supplied by the `init` command. The wallet must be
initialized and unlocked before any actions can take place.

* `siaclassicc wallet balance` prints information about your wallet.

Example:
```bash
user@hostname:~$ siaclassicc wallet balance
Wallet status:
Encrypted, Unlocked
Confirmed Balance:   61516458.00 SCA
Unconfirmed Balance: 64516461.00 SCA
Exact:               61516457999999999999999999999999 H
```

* `siaclassicc wallet address` returns a never seen before address for sending
siaclassiccoins to.

* `siaclassicc wallet send [amount] [dest]` Sends `amount` siaclassiccoins to
`dest`. `amount` is in the form XXXXUU where an X is a number and U is
a unit, for example MS, S, mS, ps, etc. If no unit is given hastings
is assumed. `dest` must be a valid siaclassiccoin address.

* `siaclassicc wallet lock` locks a wallet. After calling, the wallet must be unlocked
using the encryption password in order to use it further

* `siaclassicc wallet seeds` returns the list of secret seeds in use by the
wallet. These can be used to regenerate the wallet

* `siaclassicc wallet addseed` prompts the user for his encryption password,
as well as a new secret seed. The wallet will then incorporate this
seed into itself. This can be used for wallet recovery and merging.

#### Host tasks
* `host config [setting] [value]`

is used to configure hosting.

In version `1.2.2`, siaclassic hosting is configured as follows:

| Setting                  | Value                                           |
| -------------------------|-------------------------------------------------|
| acceptingcontracts       | Yes or No                                       |
| maxduration              | in weeks, at least 12                           |
| collateral               | in SCA / TB / Month, 10-1000                     |
| collateralbudget         | in SCA                                           |
| maxcollateral            | in SCA, max per contract                         |
| mincontractprice         | minimum price in SCA per contract                |
| mindownloadbandwidthprice| in SCA / TB                                      |
| minstorageprice          | in SCA / TB                                      |
| minuploadbandwidthprice  | in SCA / TB                                      |

You can call this many times to configure you host before
announcing. Alternatively, you can manually adjust these parameters
inside the `host/config.json` file.

* `siaclassicc host announce` makes an host announcement. You may optionally
supply a specific address to be announced; this allows you to announce a domain
name. Announcing a second time after changing settings is not necessary, as the
announcement only contains enough information to reach your host.

* `siaclassicc host -v` outputs some of your hosting settings.

Example:
```bash
user@hostname:~$ siaclassicc host -v
Host settings:
Storage:      2.0000 TB (1.524 GB used)
Price:        0.000 SCA per GB per month
Collateral:   0
Max Filesize: 10000000000
Max Duration: 8640
Contracts:    32
```

* `siaclassicc hostdb -v` prints a list of all the know active hosts on the
network.

#### Renter tasks
* `siaclassicc renter upload [filename] [nickname]` uploads a file to the siaclassic
network. `filename` is the path to the file you want to upload, and
nickname is what you will use to refer to that file in the
network. For example, it is common to have the nickname be the same as
the filename.

* `siaclassicc renter list` displays a list of the your uploaded files
currently on the siaclassic network by nickname, and their filesizes.

* `siaclassicc renter download [nickname] [destination]` downloads a file
from the siaclassic network onto your computer. `nickname` is the name used
to refer to your file in the siaclassic network, and `destination` is the
path to where the file will be. If a file already exists there, it
will be overwritten.

* `siaclassicc renter rename [nickname] [newname]` changes the nickname of a
  file.

* `siaclassicc renter delete [nickname]` removes a file from your list of
stored files. This does not remove it from the network, but only from
your saved list.

* `siaclassicc renter queue` shows the download queue. This is only relevant
if you have multiple downloads happening simultaneously.

#### Gateway tasks
* `siaclassicc gateway` prints info about the gateway, including its address and how
many peers it's connected to.

* `siaclassicc gateway list` prints a list of all currently connected peers.

* `siaclassicc gateway connect [address:port]` manually connects to a peer and adds it
to the gateway's node list.

* `siaclassicc gateway disconnect [address:port]` manually disconnects from a peer, but
leaves it in the gateway's node list.

#### Miner tasks
* `siaclassicc miner status` returns information about the miner. It is only
valid for when siaclassicd is running.

* `siaclassicc miner start` starts running the CPU miner on one thread. This
is virtually useless outside of debugging.

* `siaclassicc miner stop` halts the CPU miner.

#### General commands
* `siaclassicc consensus` prints the current block ID, current block height, and
current target.

* `siaclassicc stop` sends the stop signal to siaclassicd to safely terminate. This
has the same affect as C^c on the terminal.

* `siaclassicc version` displays the version string of siaclassicc.

* `siaclassicc update` checks the server for updates.

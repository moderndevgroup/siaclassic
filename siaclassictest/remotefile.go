package siaclassictest

import (
	"gitlab.com/moderndevgroup/SiaClassic/crypto"
)

type (
	// RemoteFile is a helper struct that represents a file uploaded to the SiaClassic
	// network.
	RemoteFile struct {
		checksum crypto.Hash
		siaclassicPath  string
	}
)

// SiaClassicPath returns the siaclassicPath of a remote file.
func (rf RemoteFile) SiaClassicPath() string {
	return rf.siaclassicPath
}

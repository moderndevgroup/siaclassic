Interested in contributing to SiaClassic?
==================================

Please review the contributing guidelines in the following pages:
- [Guide to Contributing to SiaClassic](https://gitlab.com/moderndevgroup/SiaClassic/blob/master/doc/Guide%20to%20Contributing%20to%20SiaClassic.md)
- [Developers](https://gitlab.com/moderndevgroup/SiaClassic/blob/master/doc/Developers.md)
- [All Documentation](https://gitlab.com/moderndevgroup/SiaClassic/tree/master/doc)

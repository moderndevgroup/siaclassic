package consensus

// applytransaction.go handles applying a transaction to the consensus set.
// There is an assumption that the transaction has already been verified.

import (
	"gitlab.com/moderndevgroup/SiaClassic/build"
	"gitlab.com/moderndevgroup/SiaClassic/modules"
	"gitlab.com/moderndevgroup/SiaClassic/types"

	"github.com/coreos/bbolt"
)

// applySiaClassiccoinInputs takes all of the siaclassiccoin inputs in a transaction and
// applies them to the state, updating the diffs in the processed block.
func applySiaClassiccoinInputs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	// Remove all siaclassiccoin inputs from the unspent siaclassiccoin outputs list.
	for _, sci := range t.SiaClassiccoinInputs {
		sco, err := getSiaClassiccoinOutput(tx, sci.ParentID)
		if build.DEBUG && err != nil {
			panic(err)
		}
		scod := modules.SiaClassiccoinOutputDiff{
			Direction:     modules.DiffRevert,
			ID:            sci.ParentID,
			SiaClassiccoinOutput: sco,
		}
		pb.SiaClassiccoinOutputDiffs = append(pb.SiaClassiccoinOutputDiffs, scod)
		commitSiaClassiccoinOutputDiff(tx, scod, modules.DiffApply)
	}
}

// applySiaClassiccoinOutputs takes all of the siaclassiccoin outputs in a transaction and
// applies them to the state, updating the diffs in the processed block.
func applySiaClassiccoinOutputs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	// Add all siaclassiccoin outputs to the unspent siaclassiccoin outputs list.
	for i, sco := range t.SiaClassiccoinOutputs {
		scoid := t.SiaClassiccoinOutputID(uint64(i))
		scod := modules.SiaClassiccoinOutputDiff{
			Direction:     modules.DiffApply,
			ID:            scoid,
			SiaClassiccoinOutput: sco,
		}
		pb.SiaClassiccoinOutputDiffs = append(pb.SiaClassiccoinOutputDiffs, scod)
		commitSiaClassiccoinOutputDiff(tx, scod, modules.DiffApply)
	}
}

// applyFileContracts iterates through all of the file contracts in a
// transaction and applies them to the state, updating the diffs in the proccesed
// block.
func applyFileContracts(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for i, fc := range t.FileContracts {
		fcid := t.FileContractID(uint64(i))
		fcd := modules.FileContractDiff{
			Direction:    modules.DiffApply,
			ID:           fcid,
			FileContract: fc,
		}
		pb.FileContractDiffs = append(pb.FileContractDiffs, fcd)
		commitFileContractDiff(tx, fcd, modules.DiffApply)

		// Get the portion of the contract that goes into the siaclassicfund pool and
		// add it to the siaclassicfund pool.
		sfp := getSiaClassicfundPool(tx)
		sfpd := modules.SiaClassicfundPoolDiff{
			Direction: modules.DiffApply,
			Previous:  sfp,
			Adjusted:  sfp.Add(types.Tax(blockHeight(tx), fc.Payout)),
		}
		pb.SiaClassicfundPoolDiffs = append(pb.SiaClassicfundPoolDiffs, sfpd)
		commitSiaClassicfundPoolDiff(tx, sfpd, modules.DiffApply)
	}
}

// applyTxFileContractRevisions iterates through all of the file contract
// revisions in a transaction and applies them to the state, updating the diffs
// in the processed block.
func applyFileContractRevisions(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for _, fcr := range t.FileContractRevisions {
		fc, err := getFileContract(tx, fcr.ParentID)
		if build.DEBUG && err != nil {
			panic(err)
		}

		// Add the diff to delete the old file contract.
		fcd := modules.FileContractDiff{
			Direction:    modules.DiffRevert,
			ID:           fcr.ParentID,
			FileContract: fc,
		}
		pb.FileContractDiffs = append(pb.FileContractDiffs, fcd)
		commitFileContractDiff(tx, fcd, modules.DiffApply)

		// Add the diff to add the revised file contract.
		newFC := types.FileContract{
			FileSize:           fcr.NewFileSize,
			FileMerkleRoot:     fcr.NewFileMerkleRoot,
			WindowStart:        fcr.NewWindowStart,
			WindowEnd:          fcr.NewWindowEnd,
			Payout:             fc.Payout,
			ValidProofOutputs:  fcr.NewValidProofOutputs,
			MissedProofOutputs: fcr.NewMissedProofOutputs,
			UnlockHash:         fcr.NewUnlockHash,
			RevisionNumber:     fcr.NewRevisionNumber,
		}
		fcd = modules.FileContractDiff{
			Direction:    modules.DiffApply,
			ID:           fcr.ParentID,
			FileContract: newFC,
		}
		pb.FileContractDiffs = append(pb.FileContractDiffs, fcd)
		commitFileContractDiff(tx, fcd, modules.DiffApply)
	}
}

// applyTxStorageProofs iterates through all of the storage proofs in a
// transaction and applies them to the state, updating the diffs in the processed
// block.
func applyStorageProofs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for _, sp := range t.StorageProofs {
		fc, err := getFileContract(tx, sp.ParentID)
		if build.DEBUG && err != nil {
			panic(err)
		}

		// Add all of the outputs in the ValidProofOutputs of the contract.
		for i, vpo := range fc.ValidProofOutputs {
			spoid := sp.ParentID.StorageProofOutputID(types.ProofValid, uint64(i))
			dscod := modules.DelayedSiaClassiccoinOutputDiff{
				Direction:      modules.DiffApply,
				ID:             spoid,
				SiaClassiccoinOutput:  vpo,
				MaturityHeight: pb.Height + types.MaturityDelay,
			}
			pb.DelayedSiaClassiccoinOutputDiffs = append(pb.DelayedSiaClassiccoinOutputDiffs, dscod)
			commitDelayedSiaClassiccoinOutputDiff(tx, dscod, modules.DiffApply)
		}

		fcd := modules.FileContractDiff{
			Direction:    modules.DiffRevert,
			ID:           sp.ParentID,
			FileContract: fc,
		}
		pb.FileContractDiffs = append(pb.FileContractDiffs, fcd)
		commitFileContractDiff(tx, fcd, modules.DiffApply)
	}
}

// applyTxSiaClassicfundInputs takes all of the siaclassicfund inputs in a transaction and
// applies them to the state, updating the diffs in the processed block.
func applySiaClassicfundInputs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for _, sfi := range t.SiaClassicfundInputs {
		// Calculate the volume of siaclassiccoins to put in the claim output.
		sfo, err := getSiaClassicfundOutput(tx, sfi.ParentID)
		if build.DEBUG && err != nil {
			panic(err)
		}
		claimPortion := getSiaClassicfundPool(tx).Sub(sfo.ClaimStart).Div(types.SiaClassicfundCount).Mul(sfo.Value)

		// Add the claim output to the delayed set of outputs.
		sco := types.SiaClassiccoinOutput{
			Value:      claimPortion,
			UnlockHash: sfi.ClaimUnlockHash,
		}
		sfoid := sfi.ParentID.SiaClassicClaimOutputID()
		dscod := modules.DelayedSiaClassiccoinOutputDiff{
			Direction:      modules.DiffApply,
			ID:             sfoid,
			SiaClassiccoinOutput:  sco,
			MaturityHeight: pb.Height + types.MaturityDelay,
		}
		pb.DelayedSiaClassiccoinOutputDiffs = append(pb.DelayedSiaClassiccoinOutputDiffs, dscod)
		commitDelayedSiaClassiccoinOutputDiff(tx, dscod, modules.DiffApply)

		// Create the siaclassicfund output diff and remove the output from the
		// consensus set.
		sfod := modules.SiaClassicfundOutputDiff{
			Direction:     modules.DiffRevert,
			ID:            sfi.ParentID,
			SiaClassicfundOutput: sfo,
		}
		pb.SiaClassicfundOutputDiffs = append(pb.SiaClassicfundOutputDiffs, sfod)
		commitSiaClassicfundOutputDiff(tx, sfod, modules.DiffApply)
	}
}

// applySiaClassicfundOutput applies a siaclassicfund output to the consensus set.
func applySiaClassicfundOutputs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for i, sfo := range t.SiaClassicfundOutputs {
		sfoid := t.SiaClassicfundOutputID(uint64(i))
		sfo.ClaimStart = getSiaClassicfundPool(tx)
		sfod := modules.SiaClassicfundOutputDiff{
			Direction:     modules.DiffApply,
			ID:            sfoid,
			SiaClassicfundOutput: sfo,
		}
		pb.SiaClassicfundOutputDiffs = append(pb.SiaClassicfundOutputDiffs, sfod)
		commitSiaClassicfundOutputDiff(tx, sfod, modules.DiffApply)
	}
}

// applyTransaction applies the contents of a transaction to the ConsensusSet.
// This produces a set of diffs, which are stored in the blockNode containing
// the transaction. No verification is done by this function.
func applyTransaction(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	applySiaClassiccoinInputs(tx, pb, t)
	applySiaClassiccoinOutputs(tx, pb, t)
	applyFileContracts(tx, pb, t)
	applyFileContractRevisions(tx, pb, t)
	applyStorageProofs(tx, pb, t)
	applySiaClassicfundInputs(tx, pb, t)
	applySiaClassicfundOutputs(tx, pb, t)
}

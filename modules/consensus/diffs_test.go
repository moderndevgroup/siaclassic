package consensus

import (
	"testing"

	"gitlab.com/moderndevgroup/SiaClassic/modules"
	"gitlab.com/moderndevgroup/SiaClassic/types"

	"github.com/coreos/bbolt"
)

// TestCommitDelayedSiaClassiccoinOutputDiffBadMaturity commits a delayed siaclassiccoin
// output that has a bad maturity height and triggers a panic.
func TestCommitDelayedSiaClassiccoinOutputDiffBadMaturity(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.Close()

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r == nil {
			t.Error("expecting error after corrupting database")
		}
	}()

	// Commit a delayed siaclassiccoin output with maturity height = cs.height()+1
	maturityHeight := cst.cs.dbBlockHeight() - 1
	id := types.SiaClassiccoinOutputID{'1'}
	dsco := types.SiaClassiccoinOutput{Value: types.NewCurrency64(1)}
	dscod := modules.DelayedSiaClassiccoinOutputDiff{
		Direction:      modules.DiffApply,
		ID:             id,
		SiaClassiccoinOutput:  dsco,
		MaturityHeight: maturityHeight,
	}
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		commitDelayedSiaClassiccoinOutputDiff(tx, dscod, modules.DiffApply)
		return nil
	})
}

// TestCommitNodeDiffs probes the commitNodeDiffs method of the consensus set.
/*
func TestCommitNodeDiffs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.Close()
	pb := cst.cs.dbCurrentProcessedBlock()
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		commitDiffSet(tx, pb, modules.DiffRevert) // pull the block node out of the consensus set.
		return nil
	})

	// For diffs that can be destroyed in the same block they are created,
	// create diffs that do just that. This has in the past caused issues upon
	// rewinding.
	scoid := types.SiaClassiccoinOutputID{'1'}
	scod0 := modules.SiaClassiccoinOutputDiff{
		Direction: modules.DiffApply,
		ID:        scoid,
	}
	scod1 := modules.SiaClassiccoinOutputDiff{
		Direction: modules.DiffRevert,
		ID:        scoid,
	}
	fcid := types.FileContractID{'2'}
	fcd0 := modules.FileContractDiff{
		Direction: modules.DiffApply,
		ID:        fcid,
	}
	fcd1 := modules.FileContractDiff{
		Direction: modules.DiffRevert,
		ID:        fcid,
	}
	sfoid := types.SiaClassicfundOutputID{'3'}
	sfod0 := modules.SiaClassicfundOutputDiff{
		Direction: modules.DiffApply,
		ID:        sfoid,
	}
	sfod1 := modules.SiaClassicfundOutputDiff{
		Direction: modules.DiffRevert,
		ID:        sfoid,
	}
	dscoid := types.SiaClassiccoinOutputID{'4'}
	dscod := modules.DelayedSiaClassiccoinOutputDiff{
		Direction:      modules.DiffApply,
		ID:             dscoid,
		MaturityHeight: cst.cs.dbBlockHeight() + types.MaturityDelay,
	}
	var siaclassicfundPool types.Currency
	err = cst.cs.db.Update(func(tx *bolt.Tx) error {
		siaclassicfundPool = getSiaClassicfundPool(tx)
		return nil
	})
	if err != nil {
		panic(err)
	}
	sfpd := modules.SiaClassicfundPoolDiff{
		Direction: modules.DiffApply,
		Previous:  siaclassicfundPool,
		Adjusted:  siaclassicfundPool.Add(types.NewCurrency64(1)),
	}
	pb.SiaClassiccoinOutputDiffs = append(pb.SiaClassiccoinOutputDiffs, scod0)
	pb.SiaClassiccoinOutputDiffs = append(pb.SiaClassiccoinOutputDiffs, scod1)
	pb.FileContractDiffs = append(pb.FileContractDiffs, fcd0)
	pb.FileContractDiffs = append(pb.FileContractDiffs, fcd1)
	pb.SiaClassicfundOutputDiffs = append(pb.SiaClassicfundOutputDiffs, sfod0)
	pb.SiaClassicfundOutputDiffs = append(pb.SiaClassicfundOutputDiffs, sfod1)
	pb.DelayedSiaClassiccoinOutputDiffs = append(pb.DelayedSiaClassiccoinOutputDiffs, dscod)
	pb.SiaClassicfundPoolDiffs = append(pb.SiaClassicfundPoolDiffs, sfpd)
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		createUpcomingDelayedOutputMaps(tx, pb, modules.DiffApply)
		return nil
	})
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		commitNodeDiffs(tx, pb, modules.DiffApply)
		return nil
	})
	exists := cst.cs.db.inSiaClassiccoinOutputs(scoid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	exists = cst.cs.db.inFileContracts(fcid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	exists = cst.cs.db.inSiaClassicfundOutputs(sfoid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		commitNodeDiffs(tx, pb, modules.DiffRevert)
		return nil
	})
	exists = cst.cs.db.inSiaClassiccoinOutputs(scoid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	exists = cst.cs.db.inFileContracts(fcid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	exists = cst.cs.db.inSiaClassicfundOutputs(sfoid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
}
*/

/*
// TestSiaClassiccoinOutputDiff applies and reverts a siaclassiccoin output diff, then
// triggers an inconsistency panic.
func TestCommitSiaClassiccoinOutputDiff(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.Close()

	// Commit a siaclassiccoin output diff.
	initialScosLen := cst.cs.db.lenSiaClassiccoinOutputs()
	id := types.SiaClassiccoinOutputID{'1'}
	sco := types.SiaClassiccoinOutput{Value: types.NewCurrency64(1)}
	scod := modules.SiaClassiccoinOutputDiff{
		Direction:     modules.DiffApply,
		ID:            id,
		SiaClassiccoinOutput: sco,
	}
	cst.cs.commitSiaClassiccoinOutputDiff(scod, modules.DiffApply)
	if cst.cs.db.lenSiaClassiccoinOutputs() != initialScosLen+1 {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	if cst.cs.db.getSiaClassiccoinOutputs(id).Value.Cmp(sco.Value) != 0 {
		t.Error("wrong siaclassiccoin output value after committing a diff")
	}

	// Rewind the diff.
	cst.cs.commitSiaClassiccoinOutputDiff(scod, modules.DiffRevert)
	if cst.cs.db.lenSiaClassiccoinOutputs() != initialScosLen {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	exists := cst.cs.db.inSiaClassiccoinOutputs(id)
	if exists {
		t.Error("siaclassiccoin output was not reverted")
	}

	// Restore the diff and then apply the inverse diff.
	cst.cs.commitSiaClassiccoinOutputDiff(scod, modules.DiffApply)
	scod.Direction = modules.DiffRevert
	cst.cs.commitSiaClassiccoinOutputDiff(scod, modules.DiffApply)
	if cst.cs.db.lenSiaClassiccoinOutputs() != initialScosLen {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	exists = cst.cs.db.inSiaClassiccoinOutputs(id)
	if exists {
		t.Error("siaclassiccoin output was not reverted")
	}

	// Revert the inverse diff.
	cst.cs.commitSiaClassiccoinOutputDiff(scod, modules.DiffRevert)
	if cst.cs.db.lenSiaClassiccoinOutputs() != initialScosLen+1 {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	if cst.cs.db.getSiaClassiccoinOutputs(id).Value.Cmp(sco.Value) != 0 {
		t.Error("wrong siaclassiccoin output value after committing a diff")
	}

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r != errBadCommitSiaClassiccoinOutputDiff {
			t.Error("expecting errBadCommitSiaClassiccoinOutputDiff, got", r)
		}
	}()
	// Try reverting a revert diff that was already reverted. (add an object
	// that already exists)
	cst.cs.commitSiaClassiccoinOutputDiff(scod, modules.DiffRevert)
}
*/

/*
// TestCommitFileContracttDiff applies and reverts a file contract diff, then
// triggers an inconsistency panic.
func TestCommitFileContractDiff(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Commit a file contract diff.
	initialFcsLen := cst.cs.db.lenFileContracts()
	id := types.FileContractID{'1'}
	fc := types.FileContract{Payout: types.NewCurrency64(1)}
	fcd := modules.FileContractDiff{
		Direction:    modules.DiffApply,
		ID:           id,
		FileContract: fc,
	}
	cst.cs.commitFileContractDiff(fcd, modules.DiffApply)
	if cst.cs.db.lenFileContracts() != initialFcsLen+1 {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	if cst.cs.db.getFileContracts(id).Payout.Cmp(fc.Payout) != 0 {
		t.Error("wrong siaclassiccoin output value after committing a diff")
	}

	// Rewind the diff.
	cst.cs.commitFileContractDiff(fcd, modules.DiffRevert)
	if cst.cs.db.lenFileContracts() != initialFcsLen {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	exists := cst.cs.db.inFileContracts(id)
	if exists {
		t.Error("siaclassiccoin output was not reverted")
	}

	// Restore the diff and then apply the inverse diff.
	cst.cs.commitFileContractDiff(fcd, modules.DiffApply)
	fcd.Direction = modules.DiffRevert
	cst.cs.commitFileContractDiff(fcd, modules.DiffApply)
	if cst.cs.db.lenFileContracts() != initialFcsLen {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	exists = cst.cs.db.inFileContracts(id)
	if exists {
		t.Error("siaclassiccoin output was not reverted")
	}

	// Revert the inverse diff.
	cst.cs.commitFileContractDiff(fcd, modules.DiffRevert)
	if cst.cs.db.lenFileContracts() != initialFcsLen+1 {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	if cst.cs.db.getFileContracts(id).Payout.Cmp(fc.Payout) != 0 {
		t.Error("wrong siaclassiccoin output value after committing a diff")
	}

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r != errBadCommitFileContractDiff {
			t.Error("expecting errBadCommitFileContractDiff, got", r)
		}
	}()
	// Try reverting an apply diff that was already reverted. (remove an object
	// that was already removed)
	fcd.Direction = modules.DiffApply                      // Object currently exists, but make the direction 'apply'.
	cst.cs.commitFileContractDiff(fcd, modules.DiffRevert) // revert the application.
	cst.cs.commitFileContractDiff(fcd, modules.DiffRevert) // revert the application again, in error.
}
*/

// TestSiaClassicfundOutputDiff applies and reverts a siaclassicfund output diff, then
// triggers an inconsistency panic.
/*
func TestCommitSiaClassicfundOutputDiff(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Commit a siaclassicfund output diff.
	initialScosLen := cst.cs.db.lenSiaClassicfundOutputs()
	id := types.SiaClassicfundOutputID{'1'}
	sfo := types.SiaClassicfundOutput{Value: types.NewCurrency64(1)}
	sfod := modules.SiaClassicfundOutputDiff{
		Direction:     modules.DiffApply,
		ID:            id,
		SiaClassicfundOutput: sfo,
	}
	cst.cs.commitSiaClassicfundOutputDiff(sfod, modules.DiffApply)
	if cst.cs.db.lenSiaClassicfundOutputs() != initialScosLen+1 {
		t.Error("siaclassicfund output diff set did not increase in size")
	}
	sfo1 := cst.cs.db.getSiaClassicfundOutputs(id)
	if sfo1.Value.Cmp(sfo.Value) != 0 {
		t.Error("wrong siaclassicfund output value after committing a diff")
	}

	// Rewind the diff.
	cst.cs.commitSiaClassicfundOutputDiff(sfod, modules.DiffRevert)
	if cst.cs.db.lenSiaClassicfundOutputs() != initialScosLen {
		t.Error("siaclassicfund output diff set did not increase in size")
	}
	exists := cst.cs.db.inSiaClassicfundOutputs(id)
	if exists {
		t.Error("siaclassicfund output was not reverted")
	}

	// Restore the diff and then apply the inverse diff.
	cst.cs.commitSiaClassicfundOutputDiff(sfod, modules.DiffApply)
	sfod.Direction = modules.DiffRevert
	cst.cs.commitSiaClassicfundOutputDiff(sfod, modules.DiffApply)
	if cst.cs.db.lenSiaClassicfundOutputs() != initialScosLen {
		t.Error("siaclassicfund output diff set did not increase in size")
	}
	exists = cst.cs.db.inSiaClassicfundOutputs(id)
	if exists {
		t.Error("siaclassicfund output was not reverted")
	}

	// Revert the inverse diff.
	cst.cs.commitSiaClassicfundOutputDiff(sfod, modules.DiffRevert)
	if cst.cs.db.lenSiaClassicfundOutputs() != initialScosLen+1 {
		t.Error("siaclassicfund output diff set did not increase in size")
	}
	sfo2 := cst.cs.db.getSiaClassicfundOutputs(id)
	if sfo2.Value.Cmp(sfo.Value) != 0 {
		t.Error("wrong siaclassicfund output value after committing a diff")
	}

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r != errBadCommitSiaClassicfundOutputDiff {
			t.Error("expecting errBadCommitSiaClassicfundOutputDiff, got", r)
		}
	}()
	// Try applying a revert diff that was already applied. (remove an object
	// that was already removed)
	cst.cs.commitSiaClassicfundOutputDiff(sfod, modules.DiffApply) // Remove the object.
	cst.cs.commitSiaClassicfundOutputDiff(sfod, modules.DiffApply) // Remove the object again.
}
*/

// TestCommitDelayedSiaClassiccoinOutputDiff probes the commitDelayedSiaClassiccoinOutputDiff
// method of the consensus set.
/*
func TestCommitDelayedSiaClassiccoinOutputDiff(t *testing.T) {
	t.Skip("test isn't working, but checks the wrong code anyway")
	if testing.Short() {
		t.Skip()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Commit a delayed siaclassiccoin output with maturity height = cs.height()+1
	maturityHeight := cst.cs.height() + 1
	initialDscosLen := cst.cs.db.lenDelayedSiaClassiccoinOutputsHeight(maturityHeight)
	id := types.SiaClassiccoinOutputID{'1'}
	dsco := types.SiaClassiccoinOutput{Value: types.NewCurrency64(1)}
	dscod := modules.DelayedSiaClassiccoinOutputDiff{
		Direction:      modules.DiffApply,
		ID:             id,
		SiaClassiccoinOutput:  dsco,
		MaturityHeight: maturityHeight,
	}
	cst.cs.commitDelayedSiaClassiccoinOutputDiff(dscod, modules.DiffApply)
	if cst.cs.db.lenDelayedSiaClassiccoinOutputsHeight(maturityHeight) != initialDscosLen+1 {
		t.Fatal("delayed output diff set did not increase in size")
	}
	if cst.cs.db.getDelayedSiaClassiccoinOutputs(maturityHeight, id).Value.Cmp(dsco.Value) != 0 {
		t.Error("wrong delayed siaclassiccoin output value after committing a diff")
	}

	// Rewind the diff.
	cst.cs.commitDelayedSiaClassiccoinOutputDiff(dscod, modules.DiffRevert)
	if cst.cs.db.lenDelayedSiaClassiccoinOutputsHeight(maturityHeight) != initialDscosLen {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	exists := cst.cs.db.inDelayedSiaClassiccoinOutputsHeight(maturityHeight, id)
	if exists {
		t.Error("siaclassiccoin output was not reverted")
	}

	// Restore the diff and then apply the inverse diff.
	cst.cs.commitDelayedSiaClassiccoinOutputDiff(dscod, modules.DiffApply)
	dscod.Direction = modules.DiffRevert
	cst.cs.commitDelayedSiaClassiccoinOutputDiff(dscod, modules.DiffApply)
	if cst.cs.db.lenDelayedSiaClassiccoinOutputsHeight(maturityHeight) != initialDscosLen {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	exists = cst.cs.db.inDelayedSiaClassiccoinOutputsHeight(maturityHeight, id)
	if exists {
		t.Error("siaclassiccoin output was not reverted")
	}

	// Revert the inverse diff.
	cst.cs.commitDelayedSiaClassiccoinOutputDiff(dscod, modules.DiffRevert)
	if cst.cs.db.lenDelayedSiaClassiccoinOutputsHeight(maturityHeight) != initialDscosLen+1 {
		t.Error("siaclassiccoin output diff set did not increase in size")
	}
	if cst.cs.db.getDelayedSiaClassiccoinOutputs(maturityHeight, id).Value.Cmp(dsco.Value) != 0 {
		t.Error("wrong siaclassiccoin output value after committing a diff")
	}

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r != errBadCommitDelayedSiaClassiccoinOutputDiff {
			t.Error("expecting errBadCommitDelayedSiaClassiccoinOutputDiff, got", r)
		}
	}()
	// Try applying an apply diff that was already applied. (add an object
	// that already exists)
	dscod.Direction = modules.DiffApply                             // set the direction to apply
	cst.cs.commitDelayedSiaClassiccoinOutputDiff(dscod, modules.DiffApply) // apply an already existing delayed output.
}
*/

/*
// TestCommitSiaClassicfundPoolDiff probes the commitSiaClassicfundPoolDiff method of the
// consensus set.
func TestCommitSiaClassicfundPoolDiff(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Apply two siaclassicfund pool diffs, and then a diff with 0 change. Then revert
	// them all.
	initial := cst.cs.siaclassicfundPool
	adjusted1 := initial.Add(types.NewCurrency64(200))
	adjusted2 := adjusted1.Add(types.NewCurrency64(500))
	adjusted3 := adjusted2.Add(types.NewCurrency64(0))
	sfpd1 := modules.SiaClassicfundPoolDiff{
		Direction: modules.DiffApply,
		Previous:  initial,
		Adjusted:  adjusted1,
	}
	sfpd2 := modules.SiaClassicfundPoolDiff{
		Direction: modules.DiffApply,
		Previous:  adjusted1,
		Adjusted:  adjusted2,
	}
	sfpd3 := modules.SiaClassicfundPoolDiff{
		Direction: modules.DiffApply,
		Previous:  adjusted2,
		Adjusted:  adjusted3,
	}
	cst.cs.commitSiaClassicfundPoolDiff(sfpd1, modules.DiffApply)
	if cst.cs.siaclassicfundPool.Cmp(adjusted1) != 0 {
		t.Error("siaclassicfund pool was not adjusted correctly")
	}
	cst.cs.commitSiaClassicfundPoolDiff(sfpd2, modules.DiffApply)
	if cst.cs.siaclassicfundPool.Cmp(adjusted2) != 0 {
		t.Error("second siaclassicfund pool adjustment was flawed")
	}
	cst.cs.commitSiaClassicfundPoolDiff(sfpd3, modules.DiffApply)
	if cst.cs.siaclassicfundPool.Cmp(adjusted3) != 0 {
		t.Error("second siaclassicfund pool adjustment was flawed")
	}
	cst.cs.commitSiaClassicfundPoolDiff(sfpd3, modules.DiffRevert)
	if cst.cs.siaclassicfundPool.Cmp(adjusted2) != 0 {
		t.Error("reverting second adjustment was flawed")
	}
	cst.cs.commitSiaClassicfundPoolDiff(sfpd2, modules.DiffRevert)
	if cst.cs.siaclassicfundPool.Cmp(adjusted1) != 0 {
		t.Error("reverting second adjustment was flawed")
	}
	cst.cs.commitSiaClassicfundPoolDiff(sfpd1, modules.DiffRevert)
	if cst.cs.siaclassicfundPool.Cmp(initial) != 0 {
		t.Error("reverting first adjustment was flawed")
	}

	// Do a chaining set of panics. First apply a negative pool adjustment,
	// then revert the pool diffs in the wrong order, than apply the pool diffs
	// in the wrong order.
	defer func() {
		r := recover()
		if r != errApplySiaClassicfundPoolDiffMismatch {
			t.Error("expecting errApplySiaClassicfundPoolDiffMismatch, got", r)
		}
	}()
	defer func() {
		r := recover()
		if r != errRevertSiaClassicfundPoolDiffMismatch {
			t.Error("expecting errRevertSiaClassicfundPoolDiffMismatch, got", r)
		}
		cst.cs.commitSiaClassicfundPoolDiff(sfpd1, modules.DiffApply)
	}()
	defer func() {
		r := recover()
		if r != errNonApplySiaClassicfundPoolDiff {
			t.Error(r)
		}
		cst.cs.commitSiaClassicfundPoolDiff(sfpd1, modules.DiffRevert)
	}()
	defer func() {
		r := recover()
		if r != errNegativePoolAdjustment {
			t.Error("expecting errNegativePoolAdjustment, got", r)
		}
		sfpd2.Direction = modules.DiffRevert
		cst.cs.commitSiaClassicfundPoolDiff(sfpd2, modules.DiffApply)
	}()
	cst.cs.commitSiaClassicfundPoolDiff(sfpd1, modules.DiffApply)
	cst.cs.commitSiaClassicfundPoolDiff(sfpd2, modules.DiffApply)
	negativeAdjustment := adjusted2.Sub(types.NewCurrency64(100))
	negativeSfpd := modules.SiaClassicfundPoolDiff{
		Previous: adjusted3,
		Adjusted: negativeAdjustment,
	}
	cst.cs.commitSiaClassicfundPoolDiff(negativeSfpd, modules.DiffApply)
}
*/

/*
// TestDeleteObsoleteDelayedOutputMapsSanity probes the sanity checks of the
// deleteObsoleteDelayedOutputMaps method of the consensus set.
func TestDeleteObsoleteDelayedOutputMapsSanity(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	pb := cst.cs.currentProcessedBlock()
	err = cst.cs.db.Update(func(tx *bolt.Tx) error {
		return commitDiffSet(tx, pb, modules.DiffRevert)
	})
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		r := recover()
		if r == nil {
			t.Error("expecting an error after corrupting the database")
		}
	}()
	defer func() {
		r := recover()
		if r == nil {
			t.Error("expecting an error after corrupting the database")
		}

		// Trigger a panic by deleting a map with outputs in it during revert.
		err = cst.cs.db.Update(func(tx *bolt.Tx) error {
			return createUpcomingDelayedOutputMaps(tx, pb, modules.DiffApply)
		})
		if err != nil {
			t.Fatal(err)
		}
		err = cst.cs.db.Update(func(tx *bolt.Tx) error {
			return commitNodeDiffs(tx, pb, modules.DiffApply)
		})
		if err != nil {
			t.Fatal(err)
		}
		err = cst.cs.db.Update(func(tx *bolt.Tx) error {
			return deleteObsoleteDelayedOutputMaps(tx, pb, modules.DiffRevert)
		})
		if err != nil {
			t.Fatal(err)
		}
	}()

	// Trigger a panic by deleting a map with outputs in it during apply.
	err = cst.cs.db.Update(func(tx *bolt.Tx) error {
		return deleteObsoleteDelayedOutputMaps(tx, pb, modules.DiffApply)
	})
	if err != nil {
		t.Fatal(err)
	}
}
*/

/*
// TestGenerateAndApplyDiffSanity triggers the sanity checks in the
// generateAndApplyDiff method of the consensus set.
func TestGenerateAndApplyDiffSanity(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	pb := cst.cs.currentProcessedBlock()
	cst.cs.commitDiffSet(pb, modules.DiffRevert)

	defer func() {
		r := recover()
		if r != errRegenerateDiffs {
			t.Error("expected errRegenerateDiffs, got", r)
		}
	}()
	defer func() {
		r := recover()
		if r != errInvalidSuccessor {
			t.Error("expected errInvalidSuccessor, got", r)
		}

		// Trigger errRegenerteDiffs
		_ = cst.cs.generateAndApplyDiff(pb)
	}()

	// Trigger errInvalidSuccessor
	parent := cst.cs.db.getBlockMap(pb.Parent)
	parent.DiffsGenerated = false
	_ = cst.cs.generateAndApplyDiff(parent)
}
*/

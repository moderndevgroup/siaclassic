package proto

import (
	"fmt"

	"gitlab.com/moderndevgroup/SiaClassic/crypto"
	"gitlab.com/moderndevgroup/SiaClassic/modules"
	"gitlab.com/moderndevgroup/SiaClassic/types"
)

// Dependencies.
type (
	transactionBuilder interface {
		AddFileContract(types.FileContract) uint64
		AddMinerFee(types.Currency) uint64
		AddParents([]types.Transaction)
		AddSiaClassiccoinInput(types.SiaClassiccoinInput) uint64
		AddSiaClassiccoinOutput(types.SiaClassiccoinOutput) uint64
		AddTransactionSignature(types.TransactionSignature) uint64
		FundSiaClassiccoins(types.Currency) error
		Sign(bool) ([]types.Transaction, error)
		UnconfirmedParents() ([]types.Transaction, error)
		View() (types.Transaction, []types.Transaction)
		ViewAdded() (parents, coins, funds, signatures []int)
	}

	transactionPool interface {
		AcceptTransactionSet([]types.Transaction) error
		FeeEstimation() (min types.Currency, max types.Currency)
	}

	hostDB interface {
		IncrementSuccessfulInteractions(key types.SiaClassicPublicKey)
		IncrementFailedInteractions(key types.SiaClassicPublicKey)
	}
)

// ContractParams are supplied as an argument to FormContract.
type ContractParams struct {
	Host          modules.HostDBEntry
	Funding       types.Currency
	StartHeight   types.BlockHeight
	EndHeight     types.BlockHeight
	RefundAddress types.UnlockHash
	// TODO: add optional keypair
}

// A revisionSaver is called just before we send our revision signature to the host; this
// allows the revision and Merkle roots to be reloaded later if we desync from the host.
type revisionSaver func(types.FileContractRevision, []crypto.Hash) error

// A recentRevisionError occurs if the host reports a different revision
// number than expected.
type recentRevisionError struct {
	ours, theirs uint64
}

func (e *recentRevisionError) Error() string {
	return fmt.Sprintf("our revision number (%v) does not match the host's (%v)", e.ours, e.theirs)
}

// IsRevisionMismatch returns true if err was caused by the host reporting a
// different revision number than expected.
func IsRevisionMismatch(err error) bool {
	_, ok := err.(*recentRevisionError)
	return ok
}
